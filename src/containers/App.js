import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { fetchPostsIfNeeded } from '../store/actions'

import Header from '../components/Header/Header'
import Loading from '../components/Loading'
import Posts from '../components/Posts'

import './App.css'

class App extends React.Component {
  componentDidMount() {
    this.props.dispatch(fetchPostsIfNeeded(this.props.selectedSubreddit))
  }

  render() {
    return (
      <div>
        <Header lastUpdated={this.props.lastUpdated} />

        <main className='container my-4'>
          {this.props.isFetching ? (
            <Loading />
          ) : (
            <Posts posts={this.props.posts} />
          )}
        </main>
      </div>
    )
  }
}

App.propTypes = {
  dispatch: PropTypes.func.isRequired,
  selectedSubreddit: PropTypes.string.isRequired,
  isFetching: PropTypes.bool.isRequired,
  posts: PropTypes.array.isRequired
}

const mapStateToProps = (state, props) => {
  const postsBySubreddit = state.postsBySubreddit[state.selectedSubreddit] || {
    items: [],
    isFetching: false
  }

  return {
    selectedSubreddit: state.selectedSubreddit,
    posts: postsBySubreddit.items,
    isFetching: postsBySubreddit.isFetching,
    lastUpdated: postsBySubreddit.lastUpdated
  }
}

export default connect(mapStateToProps)(App)
