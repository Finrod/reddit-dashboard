import { connect } from 'react-redux'
import { setSearch, searchPosts } from '../store/actions'

import Search from '../components/Header/Search'

const mapStateToProps = state => ({
  search: state.search
})

const mapDispatchToProps = dispatch => ({
  onInputChange: search => {
    dispatch(setSearch(search))
  },
  onSearch: () => {
    dispatch(searchPosts())
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search)
