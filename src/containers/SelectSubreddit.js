import { connect } from 'react-redux'
import {
  selectSubreddit,
  fetchPostsIfNeeded,
  setSearch
} from '../store/actions'

import Subreddit from '../components/Header/Subreddit'

const mapStateToProps = (state, props) => ({
  active: state.selectedSubreddit === props.subreddit
})

const mapDispatchToProps = (dispatch, props) => ({
  onClick: () => {
    dispatch(selectSubreddit(props.subreddit))
    dispatch(fetchPostsIfNeeded(props.subreddit))
    dispatch(setSearch(''))
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Subreddit)
