import React from 'react'
import PropTypes from 'prop-types'

class Search extends React.Component {
  handleChange = e => {
    this.props.onInputChange(e.target.value)
  }

  handleSearch = e => {
    this.props.onSearch()
    e.preventDefault()
  }

  render() {
    return (
      <form className='form-inline'>
        <input
          className='form-control form-control-sm mr-2'
          type='search'
          placeholder='Search'
          value={this.props.search}
          onChange={this.handleChange}
        />

        <button
          className='btn btn-sm btn-primary my-2'
          onClick={this.handleSearch}
        >
          Search
        </button>
      </form>
    )
  }
}

Search.propsType = {
  search: PropTypes.string.isRequired,
  onInputChange: PropTypes.func.isRequired,
  onSearch: PropTypes.func.isRequired
}

export default Search
