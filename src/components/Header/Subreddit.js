import React from 'react'
import PropTypes from 'prop-types'

import './Subreddit.css'

class HeaderLink extends React.Component {
  render() {
    return (
      <span
        className={'nav-link' + (this.props.active ? ' active' : '')}
        href={`#${this.props.subreddit}`}
        onClick={this.props.onClick}
      >
        {this.props.subreddit}
      </span>
    )
  }
}

HeaderLink.propTypes = {
  subreddit: PropTypes.string.isRequired,
  active: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired
}

export default HeaderLink
