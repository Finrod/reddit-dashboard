import React from 'react'
import PropTypes from 'prop-types'
import format from 'date-fns/format'

import SelectSubreddit from '../../containers/SelectSubreddit'
import SearchSubreddit from '../../containers/SearchSubreddit'

const subreddits = ['reactjs', 'vuejs']

class Header extends React.Component {
  render() {
    return (
      <nav className='navbar navbar-expand fixed-top navbar-dark bg-dark'>
        <div className='navbar-brand'>Reddit Dashboard</div>

        <div className='collapse navbar-collapse'>
          <nav className='nav'>
            {subreddits.map(sr => (
              <SelectSubreddit subreddit={sr} key={sr} />
            ))}
          </nav>
        </div>

        {this.props.lastUpdated && (
          <div className='mr-4 font-italic'>
            {format(this.props.lastUpdated, 'H:mm:ss')}
          </div>
        )}

        <SearchSubreddit />
      </nav>
    )
  }
}

Header.propsType = {
  lastUpdated: PropTypes.string
}

export default Header
