import React from 'react'
import PropTypes from 'prop-types'

import Post from './Post'

class Posts extends React.Component {
  render() {
    return (
      <div className='list-group'>
        {this.props.posts.map(p => (
          <Post key={p.id} post={p} />
        ))}
      </div>
    )
  }
}

Post.propsTypes = {
  posts: PropTypes.array.isRequired
}

export default Posts
