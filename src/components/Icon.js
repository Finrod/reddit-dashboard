import React from 'react'
import PropTypes from 'prop-types'

import {
  FaGlobeEurope,
  FaGithub,
  FaGitlab,
  FaMedium,
  FaReddit,
  FaTwitter,
  FaYoutube
} from 'react-icons/fa'

class Icon extends React.Component {
  render() {
    let icon = <FaGlobeEurope />

    if (this.props.domain.indexOf('self') !== -1) icon = <FaReddit />
    else if (this.props.domain.indexOf('github') !== -1) icon = <FaGithub />
    else if (this.props.domain.indexOf('gitlab') !== -1) icon = <FaGitlab />
    else if (this.props.domain.indexOf('medium') !== -1) icon = <FaMedium />
    else if (this.props.domain.indexOf('twitter') !== -1) icon = <FaTwitter />
    else if (this.props.domain.indexOf('youtube') !== -1) icon = <FaYoutube />

    return <div className='mr-3'>{icon}</div>
  }
}

Icon.propsTypes = {
  domain: PropTypes.string.isRequired
}

export default Icon
