import React from 'react'
import PropTypes from 'prop-types'
import format from 'date-fns/format'

import Icon from './Icon'

import './Post.css'

class Post extends React.Component {
  render() {
    const post = this.props.post
    const bg = post.stickied ? 'bg-primary' : ''

    return (
      <a
        href={post.url}
        className={`post list-group-item list-group-item-action p-2 d-flex justify-content-between align-items-center ${bg}`}
        target='_blank'
        rel='noopener noreferrer'
      >
        <div className='score mr-3'>{post.score}</div>
        <Icon domain={post.domain} />
        <div className='title mr-3'>{post.title}</div>
        <div className='author'>
          <small>{post.author}</small>
          <small>{format(post.created_utc * 1000, 'DD/MM HH:mm')}</small>
        </div>
      </a>
    )
  }
}

Post.propsTypes = {
  post: PropTypes.object.isRequired
}

export default Post
