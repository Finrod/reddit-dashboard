import React from 'react'
import ReactLoading from 'react-loading'

class Loading extends React.Component {
  render() {
    const primaryColor = getComputedStyle(
      document.documentElement
    ).getPropertyValue('--primary')

    return (
      <ReactLoading
        className='text-center mx-auto'
        type='cylon'
        color={primaryColor}
        width={175}
      />
    )
  }
}

export default Loading
