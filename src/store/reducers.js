import { combineReducers } from 'redux'
import {
  SELECT_SUBREDDIT,
  REQUEST_POSTS,
  RECEIVE_POSTS,
  SET_SEARCH
} from './actions'

function selectedSubreddit(state = 'reactjs', action) {
  switch (action.type) {
    case SELECT_SUBREDDIT:
      return action.subreddit
    default:
      return state
  }
}

function search(state = '', action) {
  switch (action.type) {
    case SET_SEARCH:
      return action.search
    default:
      return state
  }
}

function posts(state = { isFetching: false, items: [], search: '' }, action) {
  switch (action.type) {
    case REQUEST_POSTS:
      return Object.assign({}, state, { isFetching: true })
    case RECEIVE_POSTS:
      return Object.assign({}, state, {
        isFetching: false,
        items: action.posts,
        lastUpdated: action.receivedAt,
        search: action.search
      })
    default:
      return state
  }
}

function postsBySubreddit(state = {}, action) {
  switch (action.type) {
    case REQUEST_POSTS:
    case RECEIVE_POSTS:
      return Object.assign({}, state, {
        [action.subreddit]: posts(state[action.subreddit], action)
      })
    default:
      return state
  }
}

const rootReducer = combineReducers({
  selectedSubreddit,
  search,
  postsBySubreddit
})

export default rootReducer
