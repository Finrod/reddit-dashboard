import axios from 'axios'

export const SELECT_SUBREDDIT = 'SELECT_SUBREDDIT'
export const REQUEST_POSTS = 'REQUEST_POSTS'
export const RECEIVE_POSTS = 'RECEIVE_POSTS'
export const SET_SEARCH = 'SET_SEARCH'

export function selectSubreddit(subreddit) {
  return {
    type: SELECT_SUBREDDIT,
    subreddit
  }
}

function requestPosts(subreddit) {
  return {
    type: REQUEST_POSTS,
    subreddit
  }
}

function receivePosts(subreddit, json, search = '') {
  return {
    type: RECEIVE_POSTS,
    subreddit,
    posts: json.data.children.map(child => child.data),
    receivedAt: Date.now(),
    search
  }
}

function fetchPosts(subreddit) {
  return dispatch => {
    dispatch(requestPosts(subreddit))

    return axios
      .get(`https://www.reddit.com/r/${subreddit}.json?limit=20&raw_json=1`)
      .then(
        response => response.data,
        e => console.error('Error when getting posts', e)
      )
      .then(json => dispatch(receivePosts(subreddit, json)))
  }
}

function shouldFetchPosts(state, subreddit, search = '') {
  const posts = state.postsBySubreddit[subreddit]

  if (!posts) return true
  else if (posts.isFetching) return false
  else if (posts.search !== search) return true
  else if (Date.now() - posts.lastUpdated > 1000 * 60 * 10) return true
}

export function fetchPostsIfNeeded(subreddit) {
  return (dispatch, getState) => {
    if (shouldFetchPosts(getState(), subreddit)) dispatch(fetchPosts(subreddit))
  }
}

export function setSearch(search) {
  return {
    type: SET_SEARCH,
    search
  }
}

export function searchPosts() {
  return (dispatch, getState) => {
    const subreddit = getState().selectedSubreddit
    const search = getState().search

    if (search && shouldFetchPosts(getState(), subreddit, search)) {
      dispatch(requestPosts(subreddit))

      return axios
        .get(
          `https://www.reddit.com/r/${subreddit}/search.json?q=${search}&restrict_sr=true&limit=20&raw_json=1`
        )
        .then(
          response => response.data,
          e => console.error('Error when getting posts', e)
        )
        .then(json => dispatch(receivePosts(subreddit, json, search)))
    }
  }
}
